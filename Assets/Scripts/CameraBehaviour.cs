﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    private TankMovement tankMovement;
    [SerializeField] private Transform pivot;
    [SerializeField] private Transform cameraFollowObj;
    [SerializeField] private Vector3 offset;
    [SerializeField] public float smoothTime;
    private float cameraMoveSpeed = 20f;
    private Vector3 point;
    private Tank_Input input;
    private bool isWithArduino;
    

    // Start is called before the first frame update
    void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked;
        tankMovement = FindObjectOfType<TankMovement>();
        
        point = pivot.transform.position; // get target's coordenates
        
        input = FindObjectOfType<Tank_Input>();
        Rotation();

        
    }

    // Update is called once per frame
    void LateUpdate()
    {
            
        CameraUpdater();
    }

    private void CameraUpdater()
    {
        if (isWithArduino)
        {
            if ((input.getXTwo > tankMovement.speedrotationLimit || input.getXTwo < -tankMovement.speedrotationLimit) ||
        (input.getYTwo > tankMovement.speedrotationLimit || input.getYTwo < -tankMovement.speedrotationLimit))
            {
                Rotation();
            }
        }
        else
        {
            RotationWithMouse();
        }
        



    }
    private void RotationWithMouse()
    {
        Vector3 TargetRotation = pivot.rotation.eulerAngles;
        float yRotation = TargetRotation.y;



        float xtwoByCameraAxis = Input.GetAxis("Mouse X") /** transform.forward.z*/;
        float ytwoByCameraAxis = -Input.GetAxis("Mouse Y") /** (transform.right.x)*/;
        //Vector3 toRotate = new Vector3(/*input.getYTwo*/ytwoByCameraAxis, 0,xtwoByCameraAxis);
        Vector3 toRotate = new Vector3(/*input.getYTwo*/ytwoByCameraAxis, xtwoByCameraAxis,0);

        Quaternion targetRotation = Quaternion.LookRotation(toRotate, Vector3.back/*transform.forward*/);
        targetRotation.x = 0;
        targetRotation.z = 0;
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime);


        //set target object to follow
        Transform target = cameraFollowObj.transform;

        float step = cameraMoveSpeed * Time.deltaTime;
        transform.position = transform.rotation * new Vector3(0.0f, offset.y, offset.z) + target.position; // position in relation to the target
    }
    private void Rotation()
    {
        Vector3 TargetRotation = pivot.rotation.eulerAngles;
        float yRotation = TargetRotation.y;



        float xtwoByCameraAxis = -input.getXTwo /** transform.forward.z*/;
        float ytwoByCameraAxis = -input.getYTwo /** (transform.right.x)*/;
        Vector3 toRotate = new Vector3(/*input.getYTwo*/ytwoByCameraAxis, 0, /*input.getXTwo*/xtwoByCameraAxis);


        Quaternion targetRotation = Quaternion.LookRotation(toRotate, Vector3.back/*transform.forward*/);
        targetRotation.x = 0;
        targetRotation.z = 0;
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime);


        //set target object to follow
        Transform target = cameraFollowObj.transform;

        float step = cameraMoveSpeed * Time.deltaTime;
        transform.position = transform.rotation * new Vector3(0.0f, offset.y, offset.z) + target.position; // position in relation to the target
    }
}
