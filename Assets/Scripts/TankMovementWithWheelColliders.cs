﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Cinemachine;

public class TankMovementWithWheelColliders : MonoBehaviour
{
    public enum Axel
    {
        Front,
        Rear
    }
    [Serializable]
    public struct Wheel
    {
        public GameObject model;
        public WheelCollider wheelCollider;
        public Axel axel;

    }
    [SerializeField]private float maxsacceleration = 20.0f;
    [SerializeField] private float turnsensibility = 1.0f;

    [SerializeField] private float maxSteerAngle = 35.0f;
    private float inputX, inputY;
    [SerializeField] private List<Wheel> wheels;

    private Rigidbody rb;
    [SerializeField]private CinemachineFreeLook cinemachineFreeLookCamera;
    [SerializeField] private Transform turretTransform;
    private Camera camera;
    private bool alreadyLanched;
    [SerializeField] private GameObject projectile;
    [SerializeField] private Transform firePoint;
    [SerializeField] private float waitToLaunchAgain;
    // Start is called before the first frame update
    void Start()
    {
        camera = Camera.main;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    private void Update()
    {
        Move();
        Turn();
        TurretHandle();
        AnimatorWheels();
    }
    private void LateUpdate() // in order to calculate physics
    {
        
    }
    
    private void Move()
    {
        foreach(Wheel wheel in wheels)
        {
            wheel.wheelCollider.motorTorque = TankInputKeyboardMouse.instance.InputY * maxsacceleration * 500 * Time.deltaTime;
        }
        if(transform.rotation.x != 0 && transform.rotation.z != 0)
        {
            Vector3 temp = transform.rotation.eulerAngles;
            temp.x = 0;
            temp.z = 0;
            transform.rotation = Quaternion.Euler(temp);

        }
    }
    private void Turn()
    {
        foreach(Wheel wheel in wheels)
        {
            if(wheel.axel == Axel.Front)
            {
                var steerAngle = TankInputKeyboardMouse.instance.InputX * turnsensibility * maxSteerAngle;
                wheel.wheelCollider.steerAngle = Mathf.Lerp(wheel.wheelCollider.steerAngle, steerAngle,0.5f);
            }
        }
    }
    private void TurretHandle()
    {
        Quaternion targetRotation = camera.transform.rotation;
        targetRotation.x = 0;
        targetRotation.z = 0;
        turretTransform.rotation = Quaternion.Lerp(turretTransform.rotation, targetRotation, Time.deltaTime);
        if(Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }
    private void Shoot()
    {
        if (!alreadyLanched)
        {
            GameObject bullet = Instantiate(projectile, firePoint.position, firePoint.rotation);
            //Physics.IgnoreCollision(bc, bullet.GetComponent<BoxCollider>());
            StartCoroutine(LaunchAgain());
        }
    }
    IEnumerator LaunchAgain()
    {
        alreadyLanched = true;
        yield return new WaitForSeconds(waitToLaunchAgain); // wait a few seconds to launch again
        alreadyLanched = false;
    }
    private void AnimatorWheels()
    {
        foreach(var wheel in wheels)
        {
            Quaternion _rot;
            Vector3 _pos;
            wheel.wheelCollider.GetWorldPose(out _pos, out _rot);
            wheel.model.transform.position = _pos;
            wheel.model.transform.rotation = _rot;
        }
    }
}
