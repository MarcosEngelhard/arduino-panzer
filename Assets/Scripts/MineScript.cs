﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineScript : MonoBehaviour
{
    [SerializeField]private int damage = 25;

    [SerializeField] private float radius = 7f;
    [SerializeField] private float explosionForce = 10f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Explosion()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);

        foreach(Collider collider in colliders)
        {
            Debug.Log("Explode!!!" + collider.gameObject.name);
            Rigidbody rb = collider.GetComponent<Rigidbody>();

            if(rb != null)
            {
                rb.AddExplosionForce(explosionForce, transform.position, radius, 2f, ForceMode.Impulse);
                rb.gameObject.GetComponent<Health>().TakeDamage(damage);

               
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            
            Explosion();
            gameObject.SetActive(false);
        }
        
    }
}
