﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System;

public class Tank_Input : MonoBehaviour
{
    public Camera camera;
    public wrmhl device = new wrmhl();
    [SerializeField ]private string[] arduinoOutput = new string[6];

    SerialPort stream;

    private int x = 0;
    private int y = 0;
    private bool press = false;

    private int xTwo = 0;
    private int yTwo = 0;
    private bool pressTwo = false;


    private float inputForward;

    public int getX
    {
        get { return x; }
        
    }
    public int getY
    {

        
        get { return y; }
    }

    public bool getPress
    {
        get { return press; }
    }
    
    public int getXTwo
    {
        get { return xTwo; }
    }
    public int getYTwo
    {
        get { return yTwo; }
    }

    public bool getPressTwo
    {
        get { return pressTwo; }
    }

    public float InputForward
    {
        get { return inputForward; }
    }

    private float inputRotation;

    public float InputRotation
    {
        get { return inputRotation; }
    }

    private float inputHeadTankForward;

    public float InputHeadTankForward
    {
        get { return inputHeadTankForward; }
    }



    private float inputHeadTankRight;

    public float InputHeadTankRight
    {
        get { return inputHeadTankRight; }
    }

    // Start is called before the first frame update
    void Start()
    {
        openStream(true);
    }
    private void OnApplicationQuit() // Exit game
    {

        
        
            device.close();
    }

    // Update is called once per frame
    void Update()
    {
        
        try
        {



            string value = device.readQueue();

            if (value != null)
            {
                arduinoOutput = value.Split(','); // separete withthe ','
                //stream.BaseStream.Flush();

                x = Int16.Parse(arduinoOutput[0]);
                y = Int16.Parse(arduinoOutput[1]);
                press = Int16.Parse(arduinoOutput[2]) == 0 ? true : false;
                xTwo = Int16.Parse(arduinoOutput[3]);
                yTwo = Int16.Parse(arduinoOutput[4]);
                pressTwo = Int16.Parse(arduinoOutput[5]) == 0 ? true : false; // 0 is pressing, 1 not
            }



        }
        catch (Exception e)
        {
            Debug.Log("Error caught: " + e);
        }
        //HandleInputs();
    }
    private void HandleInputs()
    {
        inputForward = Input.GetAxis("Vertical");
        inputRotation = Input.GetAxis("Horizontal");
    }
    public bool openStream(bool open)
    {
        try
        {
            if (open)
            {
                
                
                    device.set("COM3", 9600, 100, 1); // set the values and connect it
                    
                

                    device.connect();
                    

                return true;
            }
            else
            {
                
                    device.close();

                return false;
            }
        }
        catch (Exception e)
        {
            Debug.Log("Error caught: " + e);
            return false;
        }
    }
}
