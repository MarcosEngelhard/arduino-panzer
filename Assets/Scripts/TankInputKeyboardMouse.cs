﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankInputKeyboardMouse : MonoBehaviour
{
    private float inputX;
    public float InputX
    {
        get
        {
            return inputX;
        }
    }
    private float inputY;

    public float InputY
    {
        get
        {
            return inputY;
        }
    }
    public static TankInputKeyboardMouse instance;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        inputX = Input.GetAxis("Horizontal");
        inputY = Input.GetAxis("Vertical");
    }
}
