﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Image GameOverScreen;
    [SerializeField] private Image CompleteScreen;
    private static GameManager instance;
    public bool isGameOver = false;
    public bool isCompleted = false;
    public static GameManager Instance
    {
        get { return instance; }
    }
    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        GameOverScreen.gameObject.SetActive(false);
        CompleteScreen.gameObject.SetActive(false);
        Time.timeScale = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ShowGameOverScreen()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        GameOverScreen.gameObject.SetActive(true);
        isGameOver = true;
    }
    public void ShowCompleteScreen()
    {
        CompleteScreen.gameObject.SetActive(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        isCompleted = true;
    }
    public void RestartButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
