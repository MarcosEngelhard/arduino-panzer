﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    [SerializeField] private int maxHealth = 100;
    private int currentHealth;
    [SerializeField]private Text healthText;
    private Tank_Input input;
    
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        healthText.text = "Health: " + currentHealth;
        input = GetComponent<Tank_Input>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void TakeDamage(int amountOfDamageTaken)
    {
        currentHealth -= amountOfDamageTaken;
        healthText.text = "Health: " + currentHealth;
        CheckHealth();
        
    }
    private void CheckHealth()
    {
        if(currentHealth <= 0) // the player died
        {
            currentHealth = 0;
            GameManager.Instance.ShowGameOverScreen();
            Time.timeScale = 0f;
            input.openStream(false);
        }
    }

}
