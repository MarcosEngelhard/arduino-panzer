﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    public static Wall wallInstance;
    // Start is called before the first frame update
    void Start()
    {
        wallInstance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CheckIfAllTargetsAreDown() 
    {
        Debug.Log(GameObject.FindGameObjectsWithTag("Targets").Length);
        if (GameObject.FindGameObjectsWithTag("Targets").Length <= 0) // see how many targets remains, if all targets are down open the gate
        {
            OpenTheGate();
        }
    }

    private void OpenTheGate()
    {
        gameObject.SetActive(false);
    }
}
