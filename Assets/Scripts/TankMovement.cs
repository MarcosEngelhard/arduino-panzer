﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

[RequireComponent(typeof(Rigidbody))]
public class TankMovement : MonoBehaviour
{
    [SerializeField] private bool isWithArduino;
    public enum Axel
    {
        Front, Rear,
    }

    [Serializable]
    public struct Wheel
    {
        public GameObject model;
        public WheelCollider collider;
        public Axel axel;
    }

    private Rigidbody rb;
    [SerializeField] private float tankSpeed = 10f;
    [SerializeField] private float tankRotation = 15f;
    public float speedrotationLimit = 15f;
    public Transform turretTransform;
    [SerializeField] private float turretSpeedRotation;

    [SerializeField] private GameObject projectile;
    [SerializeField] private Transform firePoint;
    private bool alreadyLanched = false;
    private float waitToLaunchAgain = 4f;

    private Tank_Input input;
    [SerializeField]private Camera camera3dPerson;
    private BoxCollider bc;
    [SerializeField] private CinemachineFreeLook cinemachineFreeLookCamera;
    
   
    private bool canChange = true;
    //[SerializeField] public List<Wheel> wheels;

    // Start is called before the first frame update
    private void Start()
    {
        int i = 0;
        rb = GetComponent<Rigidbody>();
        input = GetComponent<Tank_Input>();
        bc = GetComponent<BoxCollider>();
        
        
    }

    // Update is called once per frame
    
    private void FixedUpdate()
    {
        if (GameManager.Instance.isCompleted || GameManager.Instance.isGameOver)
            return; // if is game over or completed there's no need to run the rest
        if (rb)
        {
            Movement();
            TurrentHandle();
            //SwitchCamera();
        }

    }

    private void TurrentHandle()
    { // move only the head of the tank ( along with the canon)

        if (isWithArduino)
        {
            //if ((input.getXTwo > speedrotationLimit || input.getXTwo < -speedrotationLimit) ||
            //(input.getYTwo > speedrotationLimit || input.getYTwo < -speedrotationLimit))
            //{
            //Vector3 toRotate = new Vector3(Mathf.Atan2(-input.getYTwo, -input.getXTwo) * 180 / Mathf.PI,0 ,0);
            //float xtwoByCameraAxis = -input.getXTwo * camera.transform.forward.z;
            //float ytwoByCameraAxis = -input.getYTwo * (camera.transform.right.x);
            //Vector3 toRotate = new Vector3(/*input.getYTwo*/ytwoByCameraAxis, 0, /*input.getXTwo*/xtwoByCameraAxis);


            //Quaternion targetRotation = Quaternion.LookRotation(toRotate,/*Vector3.back*/camera.transform.forward);
            Quaternion targetRotation = cinemachineFreeLookCamera.transform.rotation;
            //targetRotation.x = 0;
            //targetRotation.z = 0;
            turretTransform.rotation = Quaternion.Lerp(turretTransform.rotation, targetRotation, Time.deltaTime);
                //turretTransform.eulerAngles = new Vector3(0,
                //Mathf.Lerp(turretTransform.eulerAngles.y, toRotate, Time.fixedDeltaTime * turretSpeedRotation), 0);
            //}
        }
        else
        {
            //Vector3 toRotate = new Vector3(Mathf.Atan2(-input.getYTwo, -input.getXTwo) * 180 / Mathf.PI,0 ,0);
            float xtwoByCameraAxis = -input.getXTwo * camera3dPerson.transform.forward.z;
            float ytwoByCameraAxis = -input.getYTwo * (camera3dPerson.transform.right.x);
            Vector3 toRotate = new Vector3(/*-input.getYTwo*/ytwoByCameraAxis, 0, /*-input.getXTwo*/xtwoByCameraAxis);


            Quaternion targetRotation = Quaternion.LookRotation(toRotate,/*Vector3.back*/camera3dPerson.transform.forward);
            targetRotation.x = 0;
            targetRotation.z = 0;
            turretTransform.rotation = Quaternion.Lerp(turretTransform.rotation, targetRotation, Time.fixedDeltaTime);
            //turretTransform.eulerAngles = new Vector3(0,
            //Mathf.Lerp(turretTransform.eulerAngles.y, toRotate, Time.fixedDeltaTime * turretSpeedRotation), 0);
        }

        if (input.getPressTwo)
        {
            
            Shoot();
        }
        
    }

    private void Shoot() // shot the projectile
    {
        if (!alreadyLanched)
        {
            GameObject bullet =  Instantiate(projectile, firePoint.position, firePoint.rotation);
            Physics.IgnoreCollision(bc, bullet.GetComponent<BoxCollider>());
            StartCoroutine(LaunchAgain());
        }
        
    }

    private void Movement()
    {

       
        // Tank is moving forward
        if (isWithArduino)
        {
            if (input.getX > speedrotationLimit || input.getX < -speedrotationLimit)
            {

                Vector3 toPosition = (transform.forward * input.getX * tankSpeed * Time.deltaTime);
                toPosition.y = 0;
                rb.velocity += toPosition;
            }
        }
        else
        {

            Vector3 toPosition = (transform.forward * input.InputForward * tankSpeed * Time.deltaTime);
            toPosition.y = 0;
            rb.velocity += toPosition;
        }

        //Rotate the tank
        if (isWithArduino)
        {
            if (input.getY > speedrotationLimit || input.getY < -speedrotationLimit)
            {
                Quaternion Rotation = transform.rotation * Quaternion.Euler(/*Vector3.up*/-camera3dPerson.transform.up * (tankRotation * input.getY * Time.fixedDeltaTime));
                rb.MoveRotation(Rotation);
            }
        }
        else
        {
            Quaternion Rotation = transform.rotation * Quaternion.Euler(/*Vector3.up*/-camera3dPerson.transform.up * (tankRotation * input.InputRotation * Time.deltaTime));
            rb.MoveRotation(Rotation);
        }
        
    }
    IEnumerator LaunchAgain()
    {
        alreadyLanched = true;
        yield return new WaitForSeconds(waitToLaunchAgain); // wait a few seconds to launch again
        alreadyLanched = false;
    }
}
