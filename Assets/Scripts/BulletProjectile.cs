﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BulletProjectile : MonoBehaviour
{
    private Rigidbody rb;
    [SerializeField] private float bulletSpeed = 10f;
    [SerializeField] private float toBeDestroyed = 4f;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        StartCoroutine(DestroyGameObject());
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = transform.forward * bulletSpeed;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Targets"))
        {
            other.gameObject.SetActive(false);
            Wall.wallInstance.CheckIfAllTargetsAreDown();
            Destroy(this.gameObject);
        }
    }
    
    IEnumerator DestroyGameObject()
    {
        yield return new WaitForSeconds(toBeDestroyed);
        
        Destroy(this.gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }
}
